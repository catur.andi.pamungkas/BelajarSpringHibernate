/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.belajar.springHibernate.controller;

import com.belajar.springHibernate.App;
import com.belajar.springHibernate.configuration.MahasiswaTableModel;
import com.belajar.springHibernate.model.Mahasiswa;
import com.belajar.springHibernate.view.MahasiswaView;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author CEKANDI
 */
public class MahasiswaController {
    private final MahasiswaView mahasiswaView;
    private MahasiswaTableModel mahasiswaTableModel;
    private List<Mahasiswa> mahasiswas;
    
    public MahasiswaController(MahasiswaView mahasiswaView){
        this.mahasiswaView = mahasiswaView;
    }
    
    public void tampilData(){
        mahasiswas = App.getMahasiswaService().getMahasiswas();
        mahasiswaTableModel = new MahasiswaTableModel(mahasiswas);
        this.mahasiswaView.getTabelMahasiswa().setModel(mahasiswaTableModel);
    }
    
    public void show(){
        int index = this.mahasiswaView.getTabelMahasiswa().getSelectedRow();
        
        this.mahasiswaView.getTxtNim().setText(String.valueOf(
            this.mahasiswaView.getTabelMahasiswa().getValueAt(index,0)));
        this.mahasiswaView.getTxtNama().setText(String.valueOf(
            this.mahasiswaView.getTabelMahasiswa().getValueAt(index,1)));
        this.mahasiswaView.getTxtKelas().setText(String.valueOf(
            this.mahasiswaView.getTabelMahasiswa().getValueAt(index,2)));
        this.mahasiswaView.getTxtAlamat().setText(String.valueOf(
            this.mahasiswaView.getTabelMahasiswa().getValueAt(index,3)));
    }
    
    public void clear(){
        this.mahasiswaView.getTxtNim().setText("");
        this.mahasiswaView.getTxtNama().setText("");
        this.mahasiswaView.getTxtKelas().setText("");
        this.mahasiswaView.getTxtAlamat().setText("");
    }

    
    public void saveMahasiswa(){
        
        if(validasiMahasiswa(mahasiswaView)){
            Mahasiswa mahasiswa = new Mahasiswa();

            mahasiswa.setNim(this.mahasiswaView.getTxtNim().getText());
            mahasiswa.setNama(this.mahasiswaView.getTxtNama().getText());
            mahasiswa.setKelas(this.mahasiswaView.getTxtKelas().getText());
            mahasiswa.setAlamat(this.mahasiswaView.getTxtAlamat().getText());

            App.getMahasiswaService().save(mahasiswa);
            JOptionPane.showMessageDialog(null,"Data Berhasil Disimpan","Informasi",
                    JOptionPane.INFORMATION_MESSAGE);
            clear();
            tampilData(); 
        }else{
            
        }
        

        
    }
    
    
    public void updateMahasiswa(){
        
        if(validasiMahasiswa(mahasiswaView)){
            Mahasiswa mahasiswa = new Mahasiswa();
        
            mahasiswa.setNim(this.mahasiswaView.getTxtNim().getText());
            mahasiswa.setNama(this.mahasiswaView.getTxtNama().getText());
            mahasiswa.setKelas(this.mahasiswaView.getTxtKelas().getText());
            mahasiswa.setAlamat(this.mahasiswaView.getTxtAlamat().getText());

            App.getMahasiswaService().update(mahasiswa);
            JOptionPane.showMessageDialog(null,"Data Berhasil Dirubah","Sukses",JOptionPane.INFORMATION_MESSAGE);
            clear();
            tampilData();
        }else{
            
        }
    }
    
    public void deleteMahasiswa(){
        
        int pilih = JOptionPane.showConfirmDialog(mahasiswaView, "Yakin Menghapus Data Terpilih ?"
                ,"Warning",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);
        
        if(pilih==JOptionPane.YES_OPTION){
            Mahasiswa mahasiswa = new Mahasiswa();
            mahasiswa.setNim(this.mahasiswaView.getTxtNim().getText());
            mahasiswa.setNama(this.mahasiswaView.getTxtNama().getText());
            mahasiswa.setKelas(this.mahasiswaView.getTxtKelas().getText());
            mahasiswa.setAlamat(this.mahasiswaView.getTxtAlamat().getText());
            App.getMahasiswaService().delete(mahasiswa);
            JOptionPane.showMessageDialog(mahasiswaView, "Berhasil Menghapus Data"
                    ,"Sukses",JOptionPane.INFORMATION_MESSAGE);
            tampilData();
        }

    }
    
    
    public boolean validasiMahasiswa(MahasiswaView view){
       String nim =  view.getTxtNim().getText();
       String nama = view.getTxtNama().getText();
       String kelas = view.getTxtKelas().getText();
       String alamat = view.getTxtAlamat().getText();
       
       if(nim.trim().equals("")){
           JOptionPane.showMessageDialog(null,"Data NIM Tidak Boleh Kosong"
                   ,"Error",JOptionPane.ERROR_MESSAGE);
           view.getTxtNim().requestFocusInWindow(); 
           return false;
            
       }else if(nama.trim().equals("")){
            JOptionPane.showMessageDialog(null,"Data Nama Tidak Boleh Kosong"
                   ,"Error",JOptionPane.ERROR_MESSAGE);
            view.getTxtNama().requestFocusInWindow();
            return false;
       }else if(kelas.trim().equals("")){
            JOptionPane.showMessageDialog(null,"Data Kelas Tidak Boleh Kosong"
                   ,"Error",JOptionPane.ERROR_MESSAGE);
            view.getTxtKelas().requestFocusInWindow();
            return false;
       }else if(alamat.trim().equals("")){
            JOptionPane.showMessageDialog(null,"Data Alamat Tidak Boleh Kosong"
                   ,"Error",JOptionPane.ERROR_MESSAGE);
            view.getTxtAlamat().requestFocusInWindow();
            return false;
       }else{
           
           return true;
       }
       
    }
    
    public void close(){
        int pilih = JOptionPane.showConfirmDialog(null,"Apakah Anda Yakin Ingin Keluar ?",
                "Keluar Program",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);
        if(pilih==JOptionPane.YES_OPTION){
            System.exit(0);
        }else{
            
        }
    }
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
