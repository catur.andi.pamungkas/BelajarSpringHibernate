/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.belajar.springHibernate.dao;

import com.belajar.springHibernate.model.Mahasiswa;
import java.util.List;
import org.springframework.stereotype.Repository;


/**
 *
 * @author CEKANDI
 */
//public interface MahasiswaDao {
//    public void save(Mahasiswa mahasiswa);
//    public void update(Mahasiswa mahasiswa);
//    public void delete(Mahasiswa mahasiswa);
//    
//    public Mahasiswa getMahasiswa(String nim);
//    public List<Mahasiswa> getMahasiswas();
//}

@Repository
public class MahasiswaDao extends BaseDaoHibernate<Mahasiswa>{
    
    public List<Mahasiswa> getMahasiswa(String nim) {
        String query = "from " + domainClass.getName();
        query += " where nim=:nim";
        return sessionFactory.getCurrentSession()
                .createQuery(query).setParameter("nim", nim)
                .list();
    }
    
    public List<Mahasiswa> getMahasiswas(){
        String query = "from " + domainClass.getName();
        return sessionFactory.getCurrentSession().createQuery(query)
        .list();
    }
    
  
    
}



