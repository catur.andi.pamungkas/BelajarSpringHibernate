/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.belajar.springHibernate;
import com.jtattoo.plaf.texture.TextureLookAndFeel;
import com.belajar.springHibernate.service.MahasiswaService;
import com.belajar.springHibernate.view.MahasiswaView;
import javax.swing.UIManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author CEKANDI
 */
public class App {
   private static ApplicationContext appContext; 
   public static void main(String[]args){
       
        try {
            UIManager.setLookAndFeel("com.jtattoo.plaf.mcwin.McWinLookAndFeel");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
      
       appContext = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
       MahasiswaView appView = new MahasiswaView();
       appView.setVisible(true); 
   }

   public static MahasiswaService getMahasiswaService(){
        return (MahasiswaService)appContext.getBean("MahasiswaService");
   }
   
}
